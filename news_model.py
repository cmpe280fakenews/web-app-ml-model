import requests
import numpy as np
import pandas as pd
from pprint import pprint
from newsapi import NewsApiClient



# Init
newsapi = NewsApiClient(api_key='1e5cd90563d64afdb550598807a3b0ec')

# /v2/top-headlines
# top_headlines = newsapi.get_top_headlines(language='en',
#                                           country='us')

# /v2/everything
all_articles = newsapi.get_everything(country='us',
                                      language='en',
                                      page=1)

# # /v2/sources
# sources = newsapi.get_sources()

pprint(all_articles)