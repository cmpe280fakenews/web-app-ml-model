from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS
from sklearn.externals import joblib
import pandas as pd
from pprint import pprint as pp
import os
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix

'''
curl example:


curl --header "Content-Type: application/json"   --request POST   --data '{"text":" Hillary Clinton and Donald Trump."}'  http://localhost:5000/article

response:
{
    "response": "False"
}

'''

filename = 'finalized_model.sav'
vocab_filename ='vocabulary.sav'
loaded_model = ''
loaded_vocab = ''

app = Flask(__name__)
api = Api(app)

CORS(app)

class FakeNews(Resource):
    def get(self, form_id):
        pass
        # temp = db.get(form_id)
        # print('value of get: {}'.format(temp))
        # return {form_id: db.get(form_id)}

    def post(self):
        '''Fetch article text from request'''
        json_data = request.get_json(force=True)
        article_text = [json_data['text']]

        '''Vectorize text for prediction'''
        vectorizer = CountVectorizer(
            analyzer = 'word',
            lowercase = False,
            vocabulary = loaded_vocab
        )
        
        features = vectorizer.fit_transform(
            article_text
        )

        ret_obj = loaded_model.predict(features)
        return {'response': str(ret_obj[0])}, 201


api.add_resource(FakeNews, '/article')



def load_standard_data():
    '''Load the data from local folders'''
    fn_df = pd.read_csv("./fake-news-detection/data.csv")
    fn_buzzfeed_fake_df = pd.read_csv("./fakenewsnet/BuzzFeed_fake_news_content.csv")
    fn_buzzfeed_real_df = pd.read_csv("./fakenewsnet/BuzzFeed_real_news_content.csv")
    fn_politifact_fake_df = pd.read_csv("./fakenewsnet/PolitiFact_fake_news_content.csv")
    fn_politifact_real_df = pd.read_csv("./fakenewsnet/PolitiFact_real_news_content.csv")

    '''Define the features to be used'''
    fn_df = fn_df[['Headline', 'Body', 'Label']]

    '''Define the format of the features for all dataframes'''
    fn_buzzfeed_fake_df = standarize_data(fn_buzzfeed_fake_df, ['title', 'text'], '0')
    fn_buzzfeed_real_df = standarize_data(fn_buzzfeed_real_df, ['title', 'text'], '1')
    fn_politifact_fake_df = standarize_data(fn_politifact_fake_df, ['title', 'text'], '0')
    fn_politifact_real_df = standarize_data(fn_politifact_real_df, ['title', 'text'], '1')

    '''Unify all data under a single dataframe'''
    data_pd = fn_buzzfeed_real_df.append(fn_buzzfeed_fake_df)
    data_pd = data_pd.append(fn_politifact_real_df)
    data_pd = data_pd.append(fn_politifact_fake_df)
    '''Rename the features and labels to create a single standard'''
    data_pd = data_pd.rename(index=str, columns={"title": "Headline", "text": "Body", "label": "Label"})
    '''finilize adding the last of the data entries'''
    data_pd = data_pd.append(fn_df)
    '''data_pd at this point is the collection of all data from the file'''
    return data_pd


def standarize_data(df, features, label):
    '''Create an extra column that define the label as given'''
    '''Example: In this case we know the data is either Fake or True. We then can define the label
       and add it as a column in the dataframe since we know all the data belongs to one of the two
       labels. Either fake or Real
    '''
    ret_obj = df[features]
    ret_obj.insert(2, 'label', label)
    return ret_obj


def transform_features(pd):
    vectorizer = CountVectorizer(
    analyzer = 'word',
    lowercase = False
    )
    features = vectorizer.fit_transform(
        pd
    )
    pp(features)
    ret_obj = features.toarray()
    return ret_obj, vectorizer.get_feature_names()

def separate_features_label(data_pd):
    '''Separate features and labels into two variables'''
    features_pd = data_pd.drop('Label', axis=1)
    label_pd = data_pd['Label']
    label_pd = label_pd.replace(1, True)
    label_pd = label_pd.replace('1', True)
    label_pd = label_pd.replace('0', False)
    label_pd = label_pd.replace(0, False)

    return features_pd, label_pd


def find_file(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return True

    return False


def main():
    data_pd = load_standard_data()
    features, labels = separate_features_label(data_pd)
    '''clean the few instances where the body is N/A'''
    features, vocabulary = transform_features(features['Body'].fillna(''))
    '''Set the split for training and testing'''
    X_train, X_test, y_train, y_test = train_test_split(
        features,
        labels,
        train_size=0.80,
        random_state=1234)

    log_model = LogisticRegression()
    log_model = log_model.fit(X=X_train, y=y_train)
    y_pred = log_model.predict(X_test)
    # save the model to disk
    joblib.dump(log_model, filename)
    joblib.dump(vocabulary, vocab_filename)
    '''Evaluation of the model'''
    print('Linear Regression for fake news')
    print(confusion_matrix(y_test, y_pred))
    print(classification_report(y_test, y_pred))
    return log_model, vocabulary


if __name__ == '__main__':
    # load the model from disk
    if find_file(filename, '.') and find_file(vocab_filename, '.'):
        loaded_model = joblib.load(filename)
        loaded_vocab = joblib.load(vocab_filename)
    else:
        loaded_model, loaded_vocab = main()

    app.run(debug=True)
